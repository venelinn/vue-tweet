export default {
    components: {        
    },
    mounted() {
    },
    computed: {
       
    },
    methods: {
        range(value) {    
            if (0 < value && value < 25) {
                return {
                    class: 'danger',
                    label: 'Bad'
                }
            }
            else if (25 <= value && value < 50) {
                return {
                    class: 'warning',
                    label: 'Low'
                }
            }
            else if (50 <= value && value < 75) {
                return {
                    class: 'brand',
                    label: 'Good'
                }
            }
        
            return {
                class: 'success',
                label: 'Great'
            }    
        }
    }
};