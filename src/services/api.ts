import axios from 'axios';

export default {
    getData() {
        return axios.get(`static/merchant.json`, {
            headers: {
                "Content-Type": "application/json"
            }
        });
    },
    getFrontPage() {
        return axios.get(`static/frontpage.json`, {
            headers: {
                "Content-Type": "application/json"
            }
        });
    },
    getOverviewScore() {
        return axios.get(`static/overviewscores.json`, {
            headers: {
                "Content-Type": "application/json"
            }
        });
    }
};
